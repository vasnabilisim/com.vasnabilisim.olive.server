package com.vasnabilisim.olive.server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.FatalException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.core.WarningException;
import com.vasnabilisim.dataconverter.DataConverter;
import com.vasnabilisim.dataconverter.UnsupportedConversionException;
import com.vasnabilisim.io.MimeType;
import com.vasnabilisim.jdbc.DataSourceInfo;
import com.vasnabilisim.jdbc.DriverInfo;
import com.vasnabilisim.jdbc.DriverInfos;
import com.vasnabilisim.olive.core.Formats;
import com.vasnabilisim.olive.server.data.handler.HandlerInfos;
import com.vasnabilisim.olive.server.xmlmodel.XmlModelType;

/**
 * @author Menderes Fatih GUVEN
 */
public class Server {
	
	String name;
	boolean initialized = false;
	org.eclipse.jetty.server.Server server = null;
	
	public Server(String name) {
		this.name = name;
		ApplicationContext.put("Olive.Server.Name", name);
		ApplicationContext.put("Olive.Server.PropertiesFile", "config/olive.server.properties");
	}
	
	public void stop() throws BaseException {
		if(server == null)
			throw new WarningException("Not running.");
		try {
			server.stop();
		} catch (Exception e) {
			Logger.fatal(e);
			throw new FatalException("Unable to stop server.", e);
		}
	}
	
	public void init() throws BaseException {
		initSession();
		initLogger();
		try {
			initMimeTypes();
			initDataSources();
			initHandlers();
			initClassLoader();
			initFormats();
		} catch(BaseException e) {
			Logger.log(e);
			throw e;
		}
		initialized = true;
	}
	
	public void start() throws BaseException {
		if(server != null)
			throw new WarningException("Allready running.");
		
		if(!initialized) {
			initSession();
			initLogger();
			try {
				initMimeTypes();
				initDataSources();
				initHandlers();
				initClassLoader();
				initFormats();
			} catch(BaseException e) {
				Logger.log(e);
				throw e;
			}
		}
		
		LinkedList<Handler> handlerList = new LinkedList<>();
		try {
			createHandlers(handlerList);
		} catch(BaseException e) {
			Logger.log(e);
			throw e;
		}

		try {
			int port = getProperty("Olive.Server.Port", Integer.class, true);
			int minThreads = getProperty("Olive.Server.MinThreads", Integer.class, true);
			int maxThreads = getProperty("Olive.Server.MaxThreads", Integer.class, true);
			long idleTimeout = getProperty("Olive.Server.IdleTimeoutMillis", Long.class, true);
			long stopTimeout = getProperty("Olive.Server.StopTimeoutMillis", Long.class, true);

			QueuedThreadPool threadPool = new QueuedThreadPool();
			threadPool.setMinThreads(minThreads);
			threadPool.setMaxThreads(maxThreads);
			server = new org.eclipse.jetty.server.Server(threadPool);
			
			ServerConnector connector = new ServerConnector(server);
			connector.setPort(port);
			connector.setIdleTimeout(idleTimeout);
			connector.setStopTimeout(stopTimeout);
			server.setConnectors(new Connector[] { connector } );
			server.setStopAtShutdown(true);
			
			if(!handlerList.isEmpty()) {
				HandlerCollection handlerCollection = new HandlerCollection();
				handlerCollection.setHandlers(handlerList.toArray(new Handler[handlerList.size()]));
				server.setHandler(handlerCollection);
			}

			server.start();
			server.join();
		} catch (Exception e) {
			FatalException fatalException = new FatalException("Unable to start server.", e);
			Logger.log(fatalException);
			throw fatalException;
		}
	}

	protected void createHandlers(List<Handler> handlerList) throws BaseException {
		String urlStaticPath = getProperty("Olive.Server.UrlStaticPath", String.class, false);
		if(urlStaticPath != null) {
			ContextHandler staticHandler = new ContextHandler(urlStaticPath);
			staticHandler.setHandler(new StaticHandler());
			handlerList.add(staticHandler);
		}
		String urlDataPath = getProperty("Olive.Server.UrlDataPath", String.class, false);
		if(urlDataPath != null) {
			ContextHandler dataHandler = new ContextHandler(urlDataPath);
			dataHandler.setHandler(new DataHandler());
			handlerList.add(dataHandler);
		}
		String urlReportXsltPath = getProperty("Olive.Server.UrlReportXsltPath", String.class, false);
		if(urlReportXsltPath != null) {
			ContextHandler reportXsltHandler = new ContextHandler(urlReportXsltPath);
			reportXsltHandler.setHandler(new ReportXsltHandler());
			handlerList.add(reportXsltHandler);
		}
		String urlSqlModelDataPath = getProperty("Olive.Server.UrlSqlModelDataPath", String.class, false);
		if(urlSqlModelDataPath != null) {
			ContextHandler modelDataHandler = new ContextHandler(urlSqlModelDataPath);
			modelDataHandler.setHandler(new SqlModelDataHandler());
			handlerList.add(modelDataHandler);
		}
	}

	void initMimeTypes() throws BaseException {
		try {
			Logger.info("MimeTypes initializing");
			MimeType.read();
			Logger.info("MimeTypes done");
		} catch (IOException e) {
			throw new FatalException(e);
		}
	}

	void initSession() throws BaseException {
		String propertiesFilePath = ApplicationContext.getString("Olive.Server.PropertiesFile");
		Properties properties = new Properties();
		try {
			properties.load(new InputStreamReader(new FileInputStream(propertiesFilePath), "UTF-8"));
		} catch (IOException e) {
			throw new FatalException(String.format("Unable to read file [%s]", propertiesFilePath), e);
		}
		for(Entry<Object, Object> entry : properties.entrySet())
			ApplicationContext.put((String)entry.getKey(), (String)entry.getValue());
	}

	void initDataSources() throws BaseException {
		Logger.info("DataSources initializing");
		try {
			DriverInfos.load(ApplicationContext.getString("Olive.DataSourceFile", true));
		} catch (SQLException e) {
			new FatalException(e);
		}
		for(DriverInfo driverInfo : DriverInfos.getInstance().getDriverInfoList()) {
			for(DataSourceInfo dataSourceInfo : driverInfo.getDataSourceInfoList()) {
				XmlModelType.putRef("DataSource-" + dataSourceInfo.getDataSourceName(), dataSourceInfo);
			}
		}
		Logger.info("DataSources done");
	}

	void initHandlers() throws BaseException {
		Logger.info("Handlers initializing");
		HandlerInfos.load();
		Logger.info("Handlers done");
	}
	
	void initLogger() throws BaseException {
		Logger.init(ApplicationContext.getString("Olive.Server.Name", true), ApplicationContext.getString("Olive.Server.LogPath", true));
		Logger.info("Logger up and running");
	}

	void initClassLoader() throws BaseException {
		Logger.info("ClassLoader initializing");
		try {
			com.vasnabilisim.util.ClassLoader.init(ApplicationContext.getString("Olive.Server.LibPath"));
		} catch (IOException e) {
			new FatalException(e);
		}
		Logger.info("ClassLoader done");
	}

	private void initFormats() {
		Logger.info("Formats initializing");
		if(ApplicationContext.contains("Olive.Format.DateTime"))
			Formats.LocalDateTimeFormatter = java.time.format.DateTimeFormatter.ofPattern(ApplicationContext.getString("Olive.Format.DateTime"));
		if(ApplicationContext.contains("Olive.Format.Date"))
			Formats.LocalDateFormatter = java.time.format.DateTimeFormatter.ofPattern(ApplicationContext.getString("Olive.Format.Date"));
		if(ApplicationContext.contains("Olive.Format.Time"))
			Formats.LocalTimeFormatter = java.time.format.DateTimeFormatter.ofPattern(ApplicationContext.getString("Olive.Format.Time"));
		Logger.info("Formats done");
	}

	protected <T> T getProperty(String propertyName, Class<T> type, boolean must) throws BaseException {
		try {
			String propertyValue = ApplicationContext.getString(propertyName, must);
			return propertyValue == null ? null : DataConverter.convertTo(propertyValue, type);
		} catch (IllegalArgumentException e) {
			throw new FatalException(String.format("Unable to find property [%s]", propertyName), e);
		} catch (UnsupportedConversionException e) {
			throw new FatalException(String.format("Property [%s] is not of type [%s]", propertyName, type.getName()), e);
		}
	}
}
