package com.vasnabilisim.olive.server;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.olive.server.data.handler.Handler;

/**
 * @author Menderes Fatih GUVEN
 */
public class DataHandler extends AbstractDataHandler {

	protected void handle(String formatName, String handlerName, Map<String, String> parameterMap, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Handler handler = null;
		try {
			handler = Handler.get(handlerName);
		} catch (BaseException e) {
			Logger.error(e);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("Unexpected exception");
			response.getWriter().close();
			return;
		}
		if(handler == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.getWriter().write(String.format("Invalid path. Unable to find [%s]", handlerName));
			response.getWriter().close();
			return;
		}
		
		//IOUtil.transfer(request.getInputStream(), "out.txt");
		//IOUtil.transfer(request.getInputStream(), System.out);
		
		handle(formatName, handler, parameterMap, baseRequest, request, response);
	}
	
}
