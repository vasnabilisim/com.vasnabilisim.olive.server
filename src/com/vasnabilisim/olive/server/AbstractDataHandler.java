package com.vasnabilisim.olive.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.server.data.handler.Handler;
import com.vasnabilisim.olive.server.data.handler.HandlerException;
import com.vasnabilisim.olive.server.data.reader.JsonReader;
import com.vasnabilisim.olive.server.data.reader.Reader;
import com.vasnabilisim.olive.server.data.reader.WsReader;
import com.vasnabilisim.olive.server.data.reader.WsdlReader;
import com.vasnabilisim.olive.server.data.reader.XmlReader;
import com.vasnabilisim.olive.server.data.reader.XsdReader;
import com.vasnabilisim.olive.server.data.writer.CsvWriter;
import com.vasnabilisim.olive.server.data.writer.JsonWriter;
import com.vasnabilisim.olive.server.data.writer.TsvWriter;
import com.vasnabilisim.olive.server.data.writer.Writer;
import com.vasnabilisim.olive.server.data.writer.WsWriter;
import com.vasnabilisim.olive.server.data.writer.WsdlWriter;
import com.vasnabilisim.olive.server.data.writer.XmlWriter;
import com.vasnabilisim.olive.server.data.writer.XsdWriter;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractDataHandler extends AbstractHandler {

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		baseRequest.setHandled(true);

		String path = baseRequest.getPathInfo();
		if(StringUtil.isEmpty(path) || path.length() == 1) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("Bad request");
			response.getWriter().close();
			return;
		}

		path = path.substring(1); //remove '/'
		int index = path.indexOf('/');
		if(index < 0) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("Invalid path");
			response.getWriter().close();
			return;
		}
		
		String formatName = path.substring(0, index);
		String handlerName = path.substring(index + 1);
		Map<String, String> parameterMap = toParameterMap(request.getParameterMap());
		handle(formatName, handlerName, parameterMap, baseRequest, request, response);
	}
	
	protected abstract void handle(String formatName, String handlerName, Map<String, String> parameterMap, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
	
	protected void handle(String formatName, Handler handler, Map<String, String> parameterMap, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		ObjInfoList parameterInfoList;
		try {
			parameterInfoList = handler.getParameterInfoList().cloneAndFill(parameterMap, false);
		} catch (BaseException e) {
			Logger.error(e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.getWriter().write("Unexpected exception.");
			response.getWriter().close();
			return;
		}

		if("POST".equals(request.getMethod())) {
			Reader reader = null;
			try {
				reader = createReader(formatName, request);
				reader.read(parameterInfoList);
			} catch (IOException e) {
				Logger.error(e);
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.getWriter().write("Unable to read content");
				response.getWriter().close();
				return;
			}
		}
		
		Writer writer = null;
		try {
			writer = createWriter(formatName, handler.getName(), response);
		} catch (IOException e) {
			Logger.error(e);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write(e.getMessage());
			response.getWriter().close();
			return;
		}

		response.setContentType(writer.getContentType());
		Obj obj;
		HandlerCallable handlerCallable = new HandlerCallable(handler, parameterInfoList);
		try {
			obj = handlerCallable.handle();
		} catch (InterruptedException e) {
			Logger.error(e);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.write(HandlerException.INTERNAL_ERROR, "Unexpected exception.");
			writer.close();
			return;
		} catch (ExecutionException e) {
			if(e.getCause() instanceof HandlerException) {
				HandlerException he = (HandlerException) e.getCause();
				Logger.error(he);
				response.setStatus(he.getResponseCode());
				writer.write(he.getErrorCode(), he.getMessage());
				writer.close();
				return;
			}
			Logger.error(e.getCause());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			writer.write(HandlerException.INTERNAL_ERROR, "Unexpected exception.");
			writer.flush();
			return;
		} catch (TimeoutException e) {
			response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
			writer.write(HandlerException.TIME_OUT, "Request timed out");
			writer.close();
			return;
		}
		
		try {
			writer.write(handler.getParameterInfoList(), obj);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			Logger.error(e);
			//TODO already wrote on writer. Its half written.
			//writer.write(HandlerException.INTERNAL_ERROR, "Unexpected error.");
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} finally {
			writer.close();
		}
	}

	static Map<String, String> toParameterMap(Map<String, String[]> requestParameters) {
		TreeMap<String, String> parameters = new TreeMap<>();
		for(Entry<String, String[]> requestParameter : requestParameters.entrySet())
			parameters.put(requestParameter.getKey(), requestParameter.getValue()[0]);
		return parameters;
	}
	
	String toString(InputStream in) throws IOException {
		InputStreamReader reader = new InputStreamReader(in, "UTF-8");
		char[] chars = new char[1024];
		int length = reader.read(chars);
		if(length > 0)
			return new String(chars, 0, length);
		return null;
	}
	
	Reader createReader(String format, HttpServletRequest request) throws IOException {
		if("json".equals(format))
			return new JsonReader(request.getInputStream());
		else if("xml".equals(format))
			return new XmlReader(request.getInputStream());
		else if("xsd".equals(format))
			return new XsdReader();
		else if("wsdl".equals(format))
			return new WsdlReader();
		else if("ws".equals(format))
			return new WsReader(request.getInputStream());
		throw new IOException(String.format("Unsupported format [%s]", format));
	}

	Writer createWriter(String format, String handlerName, HttpServletResponse response) throws IOException {
		String dataPath = ApplicationContext.getString("Olive.Server.UrlDataPath", true);
		String dataNamespace = ApplicationContext.getString("Olive.Server.Namespace", true) + dataPath;
		String dataUrl = ApplicationContext.getString("Olive.Server.Url", true) + dataPath;
		if("json".equals(format))
			return new JsonWriter(response.getOutputStream());
		else if("xml".equals(format))
			return new XmlWriter(dataNamespace, dataUrl, null, handlerName, response.getOutputStream());
		else if("xsd".equals(format))
			return new XsdWriter(dataNamespace, handlerName, response.getOutputStream());
		else if("wsdl".equals(format))
			return new WsdlWriter(dataNamespace, dataUrl, handlerName, response.getOutputStream());
		else if("ws".equals(format))
			return new WsWriter(dataNamespace, dataUrl, handlerName, response.getOutputStream());
		else if("csv".equals(format))
			return new CsvWriter(response.getOutputStream());
		else if("tsv".equals(format))
			return new TsvWriter(response.getOutputStream());
		throw new IOException(String.format("Unsupported format [%s]", format));
	}

	class HandlerCallable implements Callable<Obj> {
		Handler handler;
		ObjInfoList parameterInfoList;
		HandlerCallable(Handler handler, ObjInfoList parameterInfoList) {
			this.handler = handler;
			this.parameterInfoList = parameterInfoList;
		}
		@Override
		public Obj call() throws Exception {
			return handler.handle(parameterInfoList, new TreeMap<String, Object>());
		}
		
		public Obj handle() throws InterruptedException, ExecutionException, TimeoutException {
			return ServerExecutorService.execute(this);
		}
	}
}
