package com.vasnabilisim.olive.server.data.handler;

import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class PingHandler extends AbstractHandler {

	public PingHandler() {
		super("ping");
	}
	
	@Override
	public ObjInfoList getParameterInfoList() {
		return null;
	}
	
	@Override
	public Obj handle(ObjInfoList parameterInfoList, Map<String, Object> exstra) throws HandlerException {
		ObjInfo rootObjInfo = new ObjInfo(0, getName(), ObjType.String);
		Obj rootObj = new Obj(rootObjInfo, 0);
		return rootObj;
	}
}
