package com.vasnabilisim.olive.server.data.handler;

/**
 * @author Menderes Fatih GUVEN
 */
public class HandlerInfo {

	private String name;
	private String className;
	
	public HandlerInfo() {
	}
	
	@Override
	public String toString() {
		return name + "-" + className;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getClassName() {
		return className;
	}
	
	public void setClassName(String className) {
		this.className = className;
	}
}
