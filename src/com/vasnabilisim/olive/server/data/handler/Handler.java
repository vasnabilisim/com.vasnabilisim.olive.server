package com.vasnabilisim.olive.server.data.handler;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.core.WarningException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.server.data.handler.task.TaskTree;
import com.vasnabilisim.olive.server.xmlmodel.XmlModelType;

public interface Handler {

	String getName();
	
	void setName(String name);
	
	ObjInfoList getParameterInfoList();
	
	Obj handle(ObjInfoList parameterInfoList, Map<String, Object> exstra) throws HandlerException;

	static TreeMap<String, Handler> HANDLER_MAP = new TreeMap<>();
	
	static void register(Handler handler) {
		HANDLER_MAP.put(handler.getName(), handler);
	}
	
	static Handler get(String name) throws BaseException {
		Handler handler = HANDLER_MAP.get(name);
		if(handler != null)
			return handler;
		HandlerInfo handlerInfo = HandlerInfos.getInstance().getHandlerInfo(name);
		if(handlerInfo != null) {
			Class<?> clazz;
			try {
				clazz = com.vasnabilisim.util.ClassLoader.loadClass(handlerInfo.getClassName());
			} catch (ClassNotFoundException e) {
				throw new WarningException(String.format("Unable to find class [%s]", handlerInfo.getClassName()), e);
			}
			try {
				handler = Handler.class.cast(clazz.newInstance());
			} catch (InstantiationException | IllegalAccessException e) {
				throw new WarningException(String.format("Unable to instantiate class [%s]", handlerInfo.getClassName()), e);
			} catch (ClassCastException e) {
				throw new WarningException(String.format("Class [%s] is not a handler", handlerInfo.getClassName()), e);
			}
			handler.setName(handlerInfo.getName());
			//TODO uncomment to enable caching
			//register(handler);
			return handler;
		}

		File file = new File(ApplicationContext.getString("Olive.Server.TaskPath", true) + "/" + name + ".xml");
		if(file.exists()) {
			TaskTree taskTree = (TaskTree) XmlModelType.TaskTree.read(file);
			handler = new TaskTreeHandler(taskTree);
			handler.setName(name);
			//TODO uncomment to enable caching
			//register(handler);
			return handler;
		}
		return null;
	}
}
