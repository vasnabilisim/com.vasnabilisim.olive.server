package com.vasnabilisim.olive.server.data.handler;

import javax.servlet.http.HttpServletResponse;

import com.vasnabilisim.util.I18nString;

/**
 * @author Menderes Fatih GUVEN
 */
public class HandlerException extends Exception {
	private static final long serialVersionUID = 7529091311386878063L;

	public static final int F = 1000;

	public static final int INTERNAL_ERROR = HttpServletResponse.SC_INTERNAL_SERVER_ERROR * F;
	public static final int MISSING_RESULT = HttpServletResponse.SC_INTERNAL_SERVER_ERROR * F + 1;
	public static final int INVALID_RESULT = HttpServletResponse.SC_INTERNAL_SERVER_ERROR * F + 1;

	public static final int NOT_FOUND = HttpServletResponse.SC_NOT_FOUND * F;

	public static final int INVALID_REQUEST = HttpServletResponse.SC_BAD_REQUEST * F;
	public static final int MISSING_PARAMETER = HttpServletResponse.SC_BAD_REQUEST * F + 1;
	public static final int INVALID_PARAMETER = HttpServletResponse.SC_BAD_REQUEST * F + 2;

	public static final int TIME_OUT = HttpServletResponse.SC_REQUEST_TIMEOUT * F;
	
	public static final int USER_CODE_BEGIN = HttpServletResponse.SC_OK * F + 1;
	public static final int USER_CODE_END = HttpServletResponse.SC_OK * F + F - 1;

	int code;
	I18nString message;

	/**
	 * Default constructor.
	 */
	public HandlerException(int code) {
		this.code = code;
	}

	/**
	 * @param message
	 */
	public HandlerException(int code, String message) {
		super(message);
		this.code = code;
		this.message = new I18nString(message);
	}

	/**
	 * @param cause
	 */
	public HandlerException(int code, Throwable cause) {
		super(cause);
		this.code = code;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HandlerException(int code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
		this.message = new I18nString(message);
	}
	
	/**
	 * @param message
	 */
	public HandlerException(int code, I18nString message) {
		super(message.getValue());
		this.code = code;
		this.message = message;
	}

	/**
	 * @param message
	 * @param cause
	 */
	public HandlerException(int code, I18nString message, Throwable cause) {
		super(message.getValue(), cause);
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return code;
	}
	
	public I18nString getI18nMessage() {
		return message;
	}
	
	public int getResponseCode() {
		return code / F;
	}
	
	public int getErrorCode() {
		return (code >= USER_CODE_BEGIN && code <= USER_CODE_END) ? code % F : -1;
	}
}
