package com.vasnabilisim.olive.server.data.handler;

import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.sql.SqlModelInfo;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlModelHandler implements Handler {

	SqlModelInfo sqlModelInfo;
	
	public SqlModelHandler() {
	}
	
	public SqlModelHandler(SqlModelInfo sqlModelInfo) {
		this.sqlModelInfo = sqlModelInfo;
	}
	
	@Override
	public String getName() {
		return sqlModelInfo == null ? "?" : sqlModelInfo.getName();
	}
	
	@Override
	public void setName(String name) {
	}
	
	public SqlModelInfo getSqlModelInfo() {
		return sqlModelInfo;
	}
	
	public void setSqlModelInfo(SqlModelInfo sqlModelInfo) {
		this.sqlModelInfo = sqlModelInfo;
	}
	
	@Override
	public ObjInfoList getParameterInfoList() {
		return sqlModelInfo.getParameterInfoList();
	}
	
	@Override
	public Obj handle(ObjInfoList parameterInfoList, Map<String, Object> exstra) throws HandlerException {
		try {
			return sqlModelInfo.handle(parameterInfoList);
		} catch (BaseException e) {
			throw new HandlerException(HandlerException.INTERNAL_ERROR, "Unable to execute sql.", e);
		}
	}
}
