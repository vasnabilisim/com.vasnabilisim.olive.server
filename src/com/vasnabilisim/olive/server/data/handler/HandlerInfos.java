package com.vasnabilisim.olive.server.data.handler;

import java.io.File;
import java.util.TreeMap;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.olive.server.xmlmodel.XmlModelType;

/**
 * @author Menderes Fatih GUVEN
 */
public class HandlerInfos {

	TreeMap<String, HandlerInfo> handlerInfoMap = new TreeMap<>();
	
	public HandlerInfos() {
	}

	/**
	 * Adds given handler info.
	 * @param handlerInfo
	 * @return
	 */
	public void addHandlerInfo(HandlerInfo handlerInfo) {
		handlerInfoMap.put(handlerInfo.getName(), handlerInfo);
	}
	
	/**
	 * Returns given named handler info.
	 * @param name
	 * @return
	 */
	public HandlerInfo getHandlerInfo(String name) {
		return handlerInfoMap.get(name);
	}
	
	
	private static HandlerInfos instance = null;
	public static HandlerInfos getInstance() {
		return instance;
	}
	public static void load() throws BaseException {
		instance  = (HandlerInfos) XmlModelType.Handler.read(new File(ApplicationContext.getString("Olive.Server.HandlersFile", true)));
	}
}
