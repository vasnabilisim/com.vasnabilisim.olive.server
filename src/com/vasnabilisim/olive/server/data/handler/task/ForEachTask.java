package com.vasnabilisim.olive.server.data.handler.task;

import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.core.model.ObjValue;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ForEachTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	String collection;
	
	public ForEachTask() {
	}
	
	@Override
	public String toString() {
		return "for-each";
	}
	
	public String getCollection() {
		return collection;
	}
	
	public void setCollection(String collection) {
		this.collection = collection;
	}
	
	@Override
	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(collection == null || collection.isEmpty())
			return;
		Obj collectionObj = null;
		if(collection.startsWith("$P(") && collection.endsWith(")")) {
			String parameterName = collection.substring(3, collection.length() -1);
			ObjValue parameter;
			try {
				parameter = parameterInfoList.solve(parameterName);
			} catch (BaseException e) {
				throw new HandlerException(HandlerException.INVALID_PARAMETER, e.getMessage(), e);
			}
			if(parameter.getValue() == null)
				return;
			if(parameter.getType() != ObjType.List)
				throw new HandlerException(HandlerException.INVALID_PARAMETER, String.format("Parameter [%s] must be of type List ", parameterName));
			collectionObj = (Obj) parameter.getValue();
		}
		else if(collection.startsWith("$V(") && collection.endsWith(")")) {
			//TODO What if collection variable is result
			throw new HandlerException(HandlerException.INTERNAL_ERROR, String.format("Unsupported [%s]", collection));
		} 
		else {
			throw new HandlerException(HandlerException.INTERNAL_ERROR, String.format("Unknown [%s]", collection));
		}
		if(collectionObj == null || collectionObj.getValueCount() == 0)
			return;
		collectionObj.start();
		while(collectionObj.hasNext()) {
			collectionObj.next();
			executeChildren(parameterInfoList, resultPath, exstra);
		}
		collectionObj.stop();
	}
}
