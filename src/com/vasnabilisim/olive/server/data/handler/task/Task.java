package com.vasnabilisim.olive.server.data.handler.task;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.core.model.ObjValue;
import com.vasnabilisim.olive.server.data.handler.HandlerException;


/**
 * @author Menderes Fatih GUVEN
 */
public abstract class Task implements Serializable {
	private static final long serialVersionUID = -4041592033197197996L;

	protected ArrayList<Task> childList;
	
	protected Task() {
		childList = new ArrayList<Task>(5);
	}
	
	/**
	 * Returns the number of children.
	 * @return
	 */
	public int getChildCount() {
		return childList.size();
	}
	
	/**
	 * Returns the children.
	 * @return
	 */
	public ArrayList<Task> getChildList() {
		return childList;
	}
	
	/**
	 * Adds given task to children.
	 * @param task
	 */
	public void addChild(Task task) {
		childList.add(task);
	}
	
	/**
	 * Executes the task.
	 * @param parameterInfoList
	 * @param rootResult
	 * @param parentResult
	 * @throws HandlerException
	 */
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		return null;
	}

	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		Obj obj = executeSelf(parameterInfoList, resultPath, exstra);
		
		if(obj == null)
			executeChildren(parameterInfoList, resultPath, exstra);
		else if(obj.getInfo().getType() == ObjType.Object) {
			resultPath.addLast(obj);
			executeChildren(parameterInfoList, resultPath, exstra);
			resultPath.removeLast();
		}
		else if(obj.getInfo().getType() == ObjType.List) {
			for(Object value : obj.getValues()) {
				Obj itemObj = (Obj)value;
				resultPath.addLast(itemObj);
				executeChildren(parameterInfoList, resultPath, exstra);
				resultPath.removeLast();
			}
		}
	}

	protected void executeChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		for(Task childTask : childList) {
			try {
				childTask.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
			} catch (HandlerException e) {
				throw e;
			} catch (Exception e) {
				throw new HandlerException(HandlerException.INTERNAL_ERROR, e.getMessage(), e);
			}
		}
	}
	
	protected ObjValue solveArgument(ObjInfoList parameterInfoList, Obj parent, String expression) throws HandlerException {
		ObjValue objValue;
		String argumentName;
		if(expression.startsWith("$P(") && expression.endsWith(")")) {
			argumentName = expression.substring(3, expression.length() -1);
			try {
				objValue = parameterInfoList.solve(argumentName);
			} catch (BaseException e) {
				throw new HandlerException(HandlerException.INVALID_PARAMETER, e.getMessage(), e);
			}
		}
		else if(expression.startsWith("$V(") && expression.endsWith(")")) {
			argumentName = expression.substring(3, expression.length() -1);
			try {
				objValue = parent.solve(argumentName);
			} catch (BaseException e) {
				throw new HandlerException(HandlerException.INVALID_RESULT, e.getMessage(), e);
			}
		} 
		else {
			throw new HandlerException(HandlerException.INTERNAL_ERROR, String.format("Unknown [%s]", expression));
		}
		if(objValue.getType() == null)
			throw new HandlerException(HandlerException.INVALID_PARAMETER, String.format("Unable to solve parameter [%s]", argumentName));
		return objValue;
	}
}
