package com.vasnabilisim.olive.server.data.handler.task;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class TaskTree implements Serializable {
	private static final long serialVersionUID = -6849813570288734296L;
	
	private ObjInfoList parameterInfoList = new ObjInfoList();
	private Task root;
	
	public TaskTree() {
	}
	
	/**
	 * Returns the root task.
	 * @return
	 */
	public Task getRoot() {
		return root;
	}
	
	/**
	 * Sets the root task.
	 * @param root
	 */
	public void setRoot(Task root) {
		this.root = root;
	}
	
	/**
	 * Returns the parameter info list.
	 * @return
	 */
	public ObjInfoList getParameterInfoList() {
		return parameterInfoList;
	}
	
	/**
	 * Returns the given named parameter info. 
	 * Returns null if no match found.
	 * @param name
	 * @return
	 */
	public ObjInfo getParameterInfo(String name) {
		if(name == null)
			return null;
		for(ObjInfo parameterInfo : parameterInfoList)
			if(name.equals(parameterInfo.getFieldName()))
				return parameterInfo;
		return null;
	}
	
	/**
	 * Adds given parameter info to parameter info list.
	 * @param parameterInfo
	 */
	public void addParameterInfo(ObjInfo parameterInfo) {
		parameterInfoList.addObjInfo(parameterInfo);
	}
	
	public Obj execute(String name, ObjInfoList parameterInfoList, Map<String, Object> exstra) throws HandlerException {
		if(root == null)
			return null;
		ObjInfo rootResultInfo = new ObjInfo(root.getChildCount(), name, ObjType.Object);
		rootResultInfo.setTypeName(name);
		Obj rootResult = new Obj(rootResultInfo, root.getChildCount());
		ObjPath resultPath = new ObjPath(rootResult);
		if(exstra == null)
			exstra = new TreeMap<>();
		for(Task childTask : root.childList)
			childTask.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
		return rootResult;
	}
}
