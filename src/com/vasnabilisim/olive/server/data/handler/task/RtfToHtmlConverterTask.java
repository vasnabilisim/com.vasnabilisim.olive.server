package com.vasnabilisim.olive.server.data.handler.task;

import java.util.Iterator;
import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.server.data.handler.HandlerException;
import com.vasnabilisim.text.TextUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class RtfToHtmlConverterTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	String inName;
	String outName;
	
	public RtfToHtmlConverterTask() {
	}
	
	@Override
	public String toString() {
		return "rtf-to-html(" + inName + " -> " + outName + ")";
	}

	public String getInName() {
		return inName;
	}
	
	public void setInName(String inName) {
		this.inName = inName;
	}

	public String getOutName() {
		return outName;
	}
	
	public void setOutName(String outName) {
		this.outName = outName;
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> extra) throws HandlerException {
		Obj parentObj = resultPath.getLast();
		ObjInfo parentInfo = parentObj.getInfo();
		ObjInfo outInfo = parentInfo.getChild(outName);
		if(outInfo == null) {
			outInfo = new ObjInfo(1, outName, ObjType.String);
			parentInfo.addChild(outInfo);
		}
		String rtf = getInResult(resultPath);
		String html = TextUtil.fromRtfToHtml(rtf);
		parentObj.setValue(outInfo.getIndex(), html);

		return null;
	}
	
	protected void executeChildren(ObjInfoList parameterInfoList, ObjPath resultPath, java.util.Map<String,Object> exstra) throws HandlerException {
		super.executeChildren(parameterInfoList, resultPath, exstra);
	}
	
	String getInResult(ObjPath resultPath) {
		Iterator<Obj> iterator = resultPath.descendingIterator();
		while(iterator.hasNext()) {
			Obj obj = iterator.next();
			ObjInfo childInfo = obj.getInfo().getChild(inName);
			if(childInfo == null)
				continue;
			if(childInfo.getType() != ObjType.String)
				continue;
			return (String) obj.getValue(childInfo.getIndex());
		}
		return null;
	}
}
