package com.vasnabilisim.olive.server.data.handler.task;

import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ConditionTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	IdleTask successRoot;
	IdleTask failRoot;
	
	public ConditionTask() {
	}
	
	@Override
	public String toString() {
		return "condition";
	}

	public IdleTask getSuccessRoot() {
		return successRoot;
	}
	
	public void setSuccessRoot(IdleTask successRoot) {
		this.successRoot = successRoot;
	}
	
	public IdleTask getFailRoot() {
		return failRoot;
	}
	
	public void setFailRoot(IdleTask failRoot) {
		this.failRoot = failRoot;
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		return null;
	}
	
	@Override
	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		try {
			super.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
		} catch (HandlerException e) {
			if(failRoot != null)
				failRoot.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
			return;
		}
		if(successRoot != null)
			successRoot.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
	}
}
