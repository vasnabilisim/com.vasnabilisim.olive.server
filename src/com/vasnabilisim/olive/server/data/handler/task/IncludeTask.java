package com.vasnabilisim.olive.server.data.handler.task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjValue;
import com.vasnabilisim.olive.server.data.handler.Handler;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class IncludeTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	List<Argument> argumentList = new ArrayList<>(3);
	String source;
	
	public IncludeTask() {
	}
	
	@Override
	public String toString() {
		return "include";
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public List<Argument> getArgumentList() {
		return Collections.unmodifiableList(argumentList);
	}

	public void addArgument(Argument argument) {
		argumentList.add(argument);
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(source == null || source.isEmpty())
			return null;
		Handler handler;
		try {
			handler = Handler.get(source);
		} catch (BaseException e) {
			throw new HandlerException(0, e.getMessage());
		}
		Obj includeRoot = handler.handle(createArguments(parameterInfoList, resultPath), exstra);
		Obj parent = resultPath.getLast();
		ObjInfo parentObjInfo = parent.getInfo();
		for(ObjInfo includeObjInfo : includeRoot.getInfo().getChilds())
			parentObjInfo.addChild(includeObjInfo);
		for(Object includeValue : includeRoot.getValues())
			parent.addValue(includeValue);
		return null;
	}
	
	ObjInfoList createArguments(ObjInfoList parameterInfoList, ObjPath resultPath) throws HandlerException {
		ObjInfoList argumentInfoList = new ObjInfoList();
		for(Argument argument : argumentList) {
			ObjValue objValue = solveArgument(parameterInfoList, resultPath.getLast(), argument.value);
			//TODO may not work for objects 
			ObjInfo objInfo = new ObjInfo(0, argument.name, objValue.getType());
			objInfo.setValue(objValue.getValue());
			argumentInfoList.addObjInfo(objInfo);
		}
		return argumentInfoList;
	}
	
	public static class Argument {
		String name;
		String value;
		public Argument() {
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
}
