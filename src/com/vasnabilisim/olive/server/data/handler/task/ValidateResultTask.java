package com.vasnabilisim.olive.server.data.handler.task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ValidateResultTask extends Task {
	private static final long serialVersionUID = -4029032947138164733L;

	List<String> resultList = new ArrayList<>(3);
	Integer errorCode;
	String errorMessage;

	public ValidateResultTask() {
	}
	
	@Override
	public String toString() {
		return "validate-result";
	}

	public List<String> getResultList() {
		return resultList;
	}
	
	public void addResult(String result) {
		resultList.add(result);
	}
	
	public void setResultList(List<String> resultList) {
		this.resultList = resultList;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> extra) throws HandlerException {
		return null;
	}

	@Override
	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(!validateResults(resultPath)) {
			if(errorCode != null)
				throw new HandlerException(errorCode, errorMessage);
			return;
		}
		super.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
	}
	
	boolean validateResults(ObjPath resultPath) {
		for(String resultName : resultList) {
			if(!resultExists(resultPath, resultName))
				return false;
		}
		return true;
	}

	boolean resultExists(ObjPath resultPath, String resultName) {
		Iterator<Obj> iterator = resultPath.descendingIterator();
		while(iterator.hasNext()) {
			Obj obj = iterator.next();
			ObjInfo childInfo = obj.getInfo().getChild(resultName);
			if(childInfo != null && obj.getValue(childInfo.getIndex()) != null)
				return true;
		}
		return false;
		/*
		StringTokenizer tokenizer = new StringTokenizer(resultName, ".");
		Object resultValue = resultPath.getFirst();
		while(resultValue != null && tokenizer.hasMoreTokens()) {
			if(!(resultValue instanceof Obj))
				return false;
			String token = tokenizer.nextToken();
			Obj obj = (Obj) resultValue;
			ObjInfo objInfo = obj.getInfo().getChild(token);
			if(objInfo == null)
				return false;
			resultValue = obj.getValue(objInfo.getIndex());
		}
		return resultValue != null && resultValue != resultPath.getFirst();
		*/
	}
}
