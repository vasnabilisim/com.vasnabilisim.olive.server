package com.vasnabilisim.olive.server.data.handler.task;

import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.ObjType;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class AppendTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	ObjInfo objInfo = new ObjInfo(10, "", ObjType.Object);
	
	public AppendTask() {
	}
	
	@Override
	public String toString() {
		return objInfo.toString();
	}
	
	public String getTypeName() {
		return objInfo.getTypeName();
	}
	
	public void setTypeName(String typeName) {
		objInfo.setTypeName(typeName);
	}
	
	public String getFieldName() {
		return objInfo.getFieldName();
	}
	
	public void setFieldName(String fieldName) {
		objInfo.setFieldName(fieldName);;
	}
	
	public ObjType getType() {
		return objInfo.getType();
	}
	
	public void setType(ObjType type) {
		objInfo.setType(type);
	}
	
	public String getStringValue() {
		return objInfo.getStringValue();
	}
	
	public void setStringValue(String stringValue) {
		objInfo.setStringValue(stringValue);
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> extra) throws HandlerException {
		Object value = objInfo.getValue(true);
		Obj parentObj = resultPath.getLast();
		parentObj.getInfo().addChild(objInfo);
		parentObj.addValue(value);
		return objInfo.getType().isSimple() ? null : (Obj)value;
	}
	
	@Override
	protected void executeChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(objInfo.getType().isSimple())
			return;
		super.executeChildren(parameterInfoList, resultPath, exstra);
	}
}
