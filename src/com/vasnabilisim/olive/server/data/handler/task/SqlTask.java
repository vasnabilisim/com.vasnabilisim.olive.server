package com.vasnabilisim.olive.server.data.handler.task;

import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.jdbc.DataSourceInfo;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.core.model.sql.SqlInfo;
import com.vasnabilisim.olive.core.model.sql.SqlInfoHandler;
import com.vasnabilisim.olive.core.model.sql.SqlType;
import com.vasnabilisim.olive.server.data.handler.HandlerException;
import com.vasnabilisim.util.CloneableArrayList;
import com.vasnabilisim.util.Pair;


/**
 * @author Menderes Fatih GUVEN
 */
public class SqlTask extends Task {
	private static final long serialVersionUID = -7988772911147453339L;

	private DataSourceInfo dataSourceInfo = null;
	
	private SqlInfo sqlInfo = new SqlInfo();
	
	public SqlTask() {
	}
	
	public DataSourceInfo getDataSourceInfo() {
		return dataSourceInfo;
	}
	
	@Override
	public String toString() {
		return sqlInfo.toString();
	}
	
	public void setDataSourceInfo(DataSourceInfo dataSourceInfo) {
		this.dataSourceInfo = dataSourceInfo;
	}
	
	/**
	 * Returns the type name of the query.
	 * @return
	 */
	public String getTypeName() {
		return sqlInfo.getTypeName();
	}

	/**
	 * Sets the type name of the query.
	 * @param typeName
	 */
	public void setTypeName(String typeName) {
		sqlInfo.setTypeName(typeName);;
	}
	
	/**
	 * Returns the field name of the query.
	 * @return
	 */
	public String getFieldName() {
		return sqlInfo.getFieldName();
	}

	/**
	 * Sets the field name of the query.
	 * @param fieldName
	 */
	public void setFieldName(String fieldName) {
		sqlInfo.setFieldName(fieldName);
	}

	/**
	 * Returns the type of the query.
	 * @return
	 */
	public SqlType getType() {
		return sqlInfo.getType();
	}

	/**
	 * Sets the type of the query.
	 * @param type
	 */
	public void setType(SqlType type) {
		sqlInfo.setType(type);
	}
	
	/**
	 * Returns the variables mandatory flag.
	 * @return
	 */
	public Boolean getVariablesMandatory() {
		return sqlInfo.getVariablesMandatory();
	}
	
	/**
	 * Sets the variables mandatory flag.
	 * @param variablesMandatory
	 */
	public void setVariablesMandatory(Boolean variablesMandatory) {
		sqlInfo.setVariablesMandatory(variablesMandatory);
	}

	/**
	 * Returns the query string.
	 * @return
	 */
	public String getQuery() {
		return sqlInfo.getQuery();
	}

	/**
	 * Sets the query string.
	 * @param query
	 */
	public void setQuery(String query) {
		sqlInfo.setQuery(query);
	}

	/**
	 * Returns column info list.
	 * @return
	 */
	public CloneableArrayList<ObjInfo> getColumnList() {
		return sqlInfo.getColumnList();
	}
	
	/**
	 * Returns given named column info. 
	 * Returns null if no such column info exists.
	 * @param name
	 * @return
	 */
	public ObjInfo getColumn(String name) {
		return sqlInfo.getColumn(name);
	}
	
	/**
	 * Adds column info
	 * @param column
	 */
	public void addColumn(ObjInfo column) {
		sqlInfo.addColumn(column);
	}

	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		String handlerKey = "SqlModelHandler-" +  dataSourceInfo.getDataSourceUrl();
		SqlInfoHandler handler = (SqlInfoHandler) exstra.get(handlerKey);
		Pair<ObjInfo, Object> resultPair;
		try {
			resultPair = handler.handle(sqlInfo, parameterInfoList, resultPath.getLast());
		} catch (BaseException e) {
			throw new HandlerException(HandlerException.INTERNAL_ERROR, "Unable to execute sql.", e);
		}
		if(resultPair.getFirst() == null || resultPair.getFirst().getType().isSimple()) {
			//Since value is primitive every child of this task will be added to its parent.
			return null;
		}
		return (Obj) resultPair.getSecond();
	}
	
	@Override
	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(sqlInfo == null)
			throw new HandlerException(HandlerException.INTERNAL_ERROR, "Empty Sql");
		String handlerKey = "SqlModelHandler-" +  dataSourceInfo.getDataSourceUrl();
		SqlInfoHandler handler = (SqlInfoHandler) exstra.get(handlerKey);
		if(handler == null) {
			handler = new SqlInfoHandler(dataSourceInfo);
			exstra.put(handlerKey, handler);
		}
		else
			handler = null;
		try {
			super.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
			if(handler != null) {
				handler.commitConnection();
				exstra.remove(handlerKey);
			}
		} catch (HandlerException e) {
			if(handler != null) {
				handler.rollbackConnection();
				exstra.remove(handlerKey);
			}
			throw e;
		}
	}
}
