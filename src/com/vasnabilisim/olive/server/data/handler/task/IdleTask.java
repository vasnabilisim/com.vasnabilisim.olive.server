package com.vasnabilisim.olive.server.data.handler.task;

/**
 * @author Menderes Fatih GUVEN
 */
public class IdleTask extends Task {
	private static final long serialVersionUID = -7658612837187770440L;

	public IdleTask() {
	}

	@Override
	public String toString() {
		return "Idle";
	}
}
