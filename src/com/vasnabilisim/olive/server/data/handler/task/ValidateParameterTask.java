package com.vasnabilisim.olive.server.data.handler.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjPath;
import com.vasnabilisim.olive.server.data.handler.HandlerException;

/**
 * @author Menderes Fatih GUVEN
 */
public class ValidateParameterTask extends Task {
	private static final long serialVersionUID = 2756938851172411537L;
	
	List<String> parameterList = new ArrayList<>(3);
	Integer errorCode;
	String errorMessage;
	
	public ValidateParameterTask() {
	}
	
	@Override
	public String toString() {
		return "validate-parameter " + parameterList.toString();
	}
	
	public List<String> getParameterList() {
		return parameterList;
	}
	
	public void addParameter(String parameter) {
		parameterList.add(parameter);
	}
	
	public void setParameterList(List<String> parameterList) {
		this.parameterList = parameterList;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	@Override
	public Obj executeSelf(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> extra) throws HandlerException {
		return null;
	}
	
	@Override
	protected void executeSelfAndChildren(ObjInfoList parameterInfoList, ObjPath resultPath, Map<String, Object> exstra) throws HandlerException {
		if(!validateParameters(parameterInfoList)) {
			throw new HandlerException(
					errorCode == null ? HandlerException.INVALID_PARAMETER : errorCode, 
					errorMessage == null ? "Parameters not valid" : errorMessage
			);
		}
		super.executeSelfAndChildren(parameterInfoList, resultPath, exstra);
	}
	
	boolean validateParameters(ObjInfoList parameterInfoList) {
		for(String parameterName : parameterList)
			if(!parameterExists(parameterInfoList, parameterName))
				return false;
		return true;
	}
	
	boolean parameterExists(ObjInfoList parameterInfoList, String parameterName) {
		try {
			return parameterInfoList.solve(parameterName).getValue() != null;
		} catch (BaseException e) {
			return false;
		}
	}
}
