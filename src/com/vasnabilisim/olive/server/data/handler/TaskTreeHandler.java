package com.vasnabilisim.olive.server.data.handler;

import java.util.Map;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.server.data.handler.task.TaskTree;

/**
 * @author Menderes Fatih GUVEN
 */
public class TaskTreeHandler extends AbstractHandler {

	TaskTree taskTree;
	
	public TaskTreeHandler() {
		this.taskTree = new TaskTree();
	}
	
	public TaskTreeHandler(TaskTree taskTree) {
		this.taskTree = taskTree;
	}
	
	public TaskTree getTaskTree() {
		return taskTree;
	}
	
	public void setTaskTree(TaskTree taskTree) {
		this.taskTree = taskTree;
	}
	
	@Override
	public ObjInfoList getParameterInfoList() {
		return taskTree.getParameterInfoList();
	}
	
	@Override
	public Obj handle(ObjInfoList parameterInfoList, Map<String, Object> exstra) throws HandlerException {
		return taskTree.execute(getName(), parameterInfoList, exstra);
	}
}
