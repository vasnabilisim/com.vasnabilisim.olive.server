package com.vasnabilisim.olive.server.data.handler;


/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractHandler implements Handler {

	protected String name;
	
	protected AbstractHandler() {
	}
	
	protected AbstractHandler(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
}
