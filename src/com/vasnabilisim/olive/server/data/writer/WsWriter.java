package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class WsWriter extends XmlWriter implements Writer {

	public WsWriter(String dataNamespace, String dataUrl, String serviceName, OutputStream out) throws IOException {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, null, serviceName, out);
	}
	
	public WsWriter(String dataNamespace, String dataUrl, String serviceName, java.io.Writer out) throws IOException {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, null, serviceName, out);
	}

	protected WsWriter(String dataNamespace, String dataUrl, String serviceName, XMLStreamWriter writer) {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, null, serviceName, writer);
	}
	
	@Override
	public void write(ObjInfoList parameterInfoList, Obj obj) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("soap:Envelope");
			writer.writeAttribute("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			writer.writeAttribute("xmlns:coun", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			//writer.writeAttribute("xmlns:soap", "http://www.w3.org/2003/05/soap-envelope");
			//writer.writeAttribute("xmlns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			//writer.writeAttribute("soap:encodingStyle", "http://www.w3.org/2003/05/soap-encoding");

			writer.writeStartElement("soap:Header");
			writer.writeEndElement();

			writer.writeStartElement("soap:Body");
				writeBodyContent(obj, false);
			writer.writeEndElement();
			
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	private void writeBodyContent(Obj rootResult, boolean appendNamespace) throws XMLStreamException {
		writer.writeStartElement("coun:" + serviceName);
		if(appendNamespace) {
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xsi:schemaLocation", dataUrl + "/xsd/" + serviceName);
		}

		if(writeResult) {
			writer.writeStartElement("coun:ResultCode");
			writer.writeCharacters("0");
			writer.writeEndElement();

			writer.writeStartElement("coun:ResultMessage");
			writer.writeCharacters("Success");
			writer.writeEndElement();
		}
		
		if(rootResult != null && rootResult.getValueCount() > 0 && rootResult.getValue(0) != null) {
			Obj obj = (Obj) rootResult.getValue(0);
			ObjInfo objInfo = obj.getInfo();
			if(objInfo.getType() == ObjType.Object)
				writeObject(obj);
			else if(objInfo.getType() == ObjType.List)
				writeList(obj);
		}
		
		writer.writeEndElement();
	}

	private void writeObject(Obj resultObject) throws XMLStreamException {
		ObjInfo objInfo = resultObject.getInfo();
		writer.writeStartElement("coun:" + objInfo.getTypeName());
		for(int index=0; index<objInfo.getChildCount(); ++index) {
			ObjInfo childObjInfo = objInfo.getChild(index);
			if(childObjInfo.getExclude())
				continue;
			Object value = resultObject.getValue(childObjInfo.getIndex());
			if(value == null)
				continue;
			if(childObjInfo.getType().isSimple()) {
				writer.writeStartElement("coun:" + childObjInfo.getFieldName());
				if(value != null)
					writer.writeCharacters(childObjInfo.getType().fromObjectToString(value));
				writer.writeEndElement();
			}
			else {
				if(childObjInfo.getType() == ObjType.Object)
					writeObject((Obj) value);
				else if(childObjInfo.getType() == ObjType.List)
					writeList((Obj) value);
			}
		}
		writer.writeEndElement();
	}

	private void writeList(Obj resultObject) throws XMLStreamException {
		for(Object value : resultObject.getValues()) {
			writeObject((Obj)value);
		}
	}
	
	@Override
	public void write(int code, String message) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("soap:Envelope");
			writer.writeAttribute("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
			writer.writeAttribute("xmlns:coun", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			//writer.writeAttribute("xmlns:soap", "http://www.w3.org/2003/05/soap-envelope");
			//writer.writeAttribute("xmlns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			//writer.writeAttribute("soap:encodingStyle", "http://www.w3.org/2003/05/soap-encoding");

			writer.writeStartElement("soap:Header");
			writer.writeEndElement();

			writer.writeStartElement("soap:Body");
				writer.writeStartElement("coun:" + serviceName);
				if(writeResult) {
					writer.writeStartElement("coun:ResultCode");
					writer.writeCharacters(String.valueOf(code));
					writer.writeEndElement();
					writer.writeStartElement("coun:ResultMessage");
					writer.writeCharacters(message);
					writer.writeEndElement();
				}
				writer.writeEndElement();
			writer.writeEndElement();
			
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
}
