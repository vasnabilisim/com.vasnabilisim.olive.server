package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import com.vasnabilisim.olive.core.Formats;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class CsvWriter extends AbstractWriter {

	protected com.vasnabilisim.io.CsvWriter writer;
	
	public CsvWriter(OutputStream out) throws IOException {
		this(new com.vasnabilisim.io.CsvWriter(new OutputStreamWriter(out, "UTF-8")));
	}
	
	public CsvWriter(com.vasnabilisim.io.CsvWriter out) throws IOException {
		this("text/csv;charset=utf-8", "UTF-8", out);
	}

	public CsvWriter(String contentType, String characterEncoding, com.vasnabilisim.io.CsvWriter out) throws IOException {
		super(contentType, characterEncoding);
		writer = out;
		writer.setSeperator(',');
	}

	@Override
	public void write(ObjInfoList parameterInfoList, Obj rootObj) throws IOException {
		ObjInfo rootInfo = rootObj.getInfo();
		if(rootInfo.getChildCount() > 0 && !rootInfo.getChild(0).getType().isSimple()) {
			Obj obj = (Obj) rootObj.getValue(0);
			if(obj.getInfo().getType() == ObjType.Object)
				writeObject(obj);
			else if(obj.getInfo().getType() == ObjType.List)
				writeList(obj);
		}
	}
	
	private void writeObject(Obj obj) throws IOException {
		ObjInfo objInfo = obj.getInfo();
		for(int index=0; index<objInfo.getChildCount(); ++index) {
			ObjInfo childInfo = objInfo.getChild(index);
			if(childInfo.getExclude())
				continue;
			writer.write(toString(childInfo, obj.getValue(childInfo.getIndex())));
		}
	}
	
	private void writeList(Obj obj) throws IOException {
		for(Object row : obj.getValues()) {
			writeObject((Obj) row);
			writer.nextLine();
		}
	}
	
	private String toString(ObjInfo objInfo, Object value) {
		if(value == null)
			return null;
		switch(objInfo.getType()) {
		case String:	return (String)value;
		case Integer:
		case Decimal:	
		case Boolean:	return value.toString();
		case DateTime:	return Formats.formatLocalDateTime((LocalDateTime)value);
		case Date:		return Formats.formatLocalDate((LocalDate)value);
		case Time:		return Formats.formatLocalTime((LocalTime)value);
		case Object:	
		case List:		return toString((Obj) value);
		default:		return null;
		}
	}
	
	private String toString(Obj obj) {
		ObjInfo objInfo = obj.getInfo();
		if(objInfo.getType() == ObjType.Object) {
			StringBuilder builderObject = new StringBuilder(100);
			builderObject.append('{');
			for(int index=0; index<objInfo.getChildCount(); ++index) {
				ObjInfo childInfo = objInfo.getChild(index);
				if(childInfo.getExclude())
					continue;
				if(builderObject.length() > 1)
					builderObject.append('#');
				Object childValue = obj.getValue(childInfo.getIndex());
				builderObject.append(toString(childInfo, childValue));
			}
			builderObject.append('}');
			return builderObject.toString();
		}
		else if(objInfo.getType() == ObjType.List) {
			StringBuilder builderList = new StringBuilder(100);
			builderList.append('[');
			for(Object childValue : obj.getValues()) {
				if(builderList.length() > 1)
					builderList.append('#');
				builderList.append(toString((Obj)childValue));
			}
			builderList.append(']');
			return builderList.toString();
		}
		return null;
	}
	
	@Override
	public void write(int code, String message) throws IOException {
		writer.write(String.valueOf(code));
		writer.write(message);
		writer.flush();
	}
	
	@Override
	public void flush() throws IOException {
		writer.flush();
	}

	@Override
	public void close() throws IOException {
		writer.close();
	}
}
