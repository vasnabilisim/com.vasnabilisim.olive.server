package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Locale;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlWriter extends AbstractWriter {

	protected String dataNamespace;
	protected String dataUrl;
	protected String xsltUrl;
	protected String serviceName;
	protected XMLStreamWriter writer;
	
	public XmlWriter(String dataNamespace, String dataUrl, String xsltUrl, String serviceName, OutputStream out) throws IOException {
		this("text/xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, xsltUrl, serviceName, out);
	}
	
	protected XmlWriter(String contentType, String characterEncoding, String dataNamespace, String dataUrl, String xsltUrl, String serviceName, OutputStream out) throws IOException {
		this(contentType, characterEncoding, dataNamespace, dataUrl, xsltUrl, serviceName, new OutputStreamWriter(out, characterEncoding));
	}
	
	public XmlWriter(String dataNamespace, String dataUrl, String xsltUrl, String serviceName, java.io.Writer out) throws IOException {
		this("text/xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, xsltUrl, serviceName, out);
	}

	protected XmlWriter(String contentType, String characterEncoding, String dataNamespace, String dataUrl, String xsltUrl, String serviceName, java.io.Writer out) throws IOException {
		super(contentType, characterEncoding);
		this.dataNamespace = dataNamespace;
		this.dataUrl = dataUrl;
		this.xsltUrl = xsltUrl;
		this.serviceName = serviceName;
		try {
			writer = XMLOutputFactory.newInstance().createXMLStreamWriter(out);
			//writer = new IndentingXMLStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(out));
		} catch (XMLStreamException | FactoryConfigurationError e) {
			throw new IOException(e);
		}
	}

	public XmlWriter(String dataNamespace, String dataUrl, String xsltUrl, String serviceName, XMLStreamWriter writer) {
		this("text/xml;charset=utf-8", "UTF-8", dataNamespace, dataUrl, xsltUrl, serviceName, writer);
	}

	protected XmlWriter(String contentType, String characterEncoding, String dataNamespace, String dataUrl, String xsltUrl, String serviceName, XMLStreamWriter writer) {
		super(contentType, characterEncoding);
		this.dataNamespace = dataNamespace;
		this.dataUrl = dataUrl;
		this.xsltUrl = xsltUrl;
		this.serviceName = serviceName;
		this.writer = writer;
	}
	
	@Override
	public void write(ObjInfoList parameterInfoList, Obj obj) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			if(xsltUrl != null)
				writer.writeProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"" + xsltUrl + "\"");
			write(obj, true);
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	protected void write(Obj rootResult, boolean appendNamespace) throws XMLStreamException {
		writer.writeStartElement(serviceName);
		if(appendNamespace) {
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xsi:schemaLocation", dataUrl + "/xsd/" + serviceName);
		}

		if(writeResult) {
			writer.writeStartElement("ResultCode");
			writer.writeCharacters("0");
			writer.writeEndElement();

			writer.writeStartElement("ResultMessage");
			writer.writeCharacters("Success");
			writer.writeEndElement();
		}
		
		writeObjectContent(rootResult);
		
		writer.writeEndElement();
	}

	private void writeObject(Obj resultObject) throws XMLStreamException {
		writer.writeStartElement(resultObject.getInfo().getTypeName());
		writeObjectContent(resultObject);
		writer.writeEndElement();
	}


	private void writeObjectContent(Obj resultObject) throws XMLStreamException {
		ObjInfo objInfo = resultObject.getInfo();
		for(int index=0; index<objInfo.getChildCount(); ++index) {
			ObjInfo childObjInfo = objInfo.getChild(index);
			if(childObjInfo.getExclude())
				continue;
			Object value = resultObject.getValue(childObjInfo.getIndex());
			if(value == null)
				continue;
			if(childObjInfo.getType().isSimple()) {
				writer.writeStartElement(childObjInfo.getFieldName());
				if(value != null)
					writer.writeCharacters(childObjInfo.getType().fromObjectToString(value));
				writer.writeEndElement();
			}
			else {
				if(childObjInfo.getType() == ObjType.Object)
					writeObject((Obj) value);
				else if(childObjInfo.getType() == ObjType.List)
					writeList((Obj) value);
			}
		}
	}

	private void writeList(Obj resultObject) throws XMLStreamException {
		for(Object value : resultObject.getValues()) {
			writeObject((Obj)value);
		}
	}
	
	@Override
	public void write(int code, String message) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement(serviceName);
			writer.writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xmlns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xsi:schemaLocation", dataUrl + "/xsd/" + serviceName);

			if(writeResult) {
				writer.writeStartElement("ResultCode");
				writer.writeCharacters(String.valueOf(code));
				writer.writeEndElement();
	
				writer.writeStartElement("ResultMessage");
				writer.writeCharacters(message);
				writer.writeEndElement();
			}
			
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	@Override
	public void flush() throws IOException {
		try {
			writer.flush();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			writer.close();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
}
