package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Writer {

	String getContentType();
	
	String getCharacterEncoding();

	boolean getWriteResult();
	
	void setWriteResult(boolean writeResult);
	
	void write(ObjInfoList parameterInfoList, Obj obj) throws IOException;

	void write(int code, String message) throws IOException;

	public void flush() throws IOException;

	public void close() throws IOException;

}
