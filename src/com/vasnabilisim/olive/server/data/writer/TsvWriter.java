package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author Menderes Fatih GUVEN
 */
public class TsvWriter extends CsvWriter implements Writer {

	public TsvWriter(OutputStream out) throws IOException {
		this(new com.vasnabilisim.io.CsvWriter(new OutputStreamWriter(out, "UTF-8")));
	}
	
	public TsvWriter(com.vasnabilisim.io.CsvWriter out) throws IOException {
		super("text/tab-separated-values;charset=utf-8", "UTF-8", out);
		writer.setSeperator('\t');
	}
}
