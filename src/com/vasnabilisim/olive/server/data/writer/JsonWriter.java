package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Base64;

import com.vasnabilisim.dataconverter.DataConverter;
import com.vasnabilisim.olive.core.Formats;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public class JsonWriter extends AbstractWriter {

	protected com.google.gson.stream.JsonWriter writer;
	
	public JsonWriter(OutputStream out) throws IOException {
		this(new OutputStreamWriter(out, "UTF-8"));
	}
	
	public JsonWriter(java.io.Writer out) throws IOException {
		this(new com.google.gson.stream.JsonWriter(out));
	}

	protected JsonWriter(com.google.gson.stream.JsonWriter writer) {
		super("text/json;charset=utf-8", "UTF-8");
		writer.setIndent("    ");
		this.writer = writer;
	}
	
	@Override
	public void write(ObjInfoList parameterInfoList, Obj rootResultObject) throws IOException {
		writer.beginObject();
		if(writeResult) {
			writer.name("ResultCode").value(0);
			writer.name("ResultMessage").value("Success");
		}
		writeObjectContent(rootResultObject);
		writer.endObject();
	}
	
	private void writeObject(Obj resultObject) throws IOException {
		writer.beginObject();
		writeObjectContent(resultObject);
		writer.endObject();
	}

	private void writeObjectContent(Obj resultObject) throws IOException {
		ObjInfo objInfo = resultObject.getInfo();
		for(int index=0; index<objInfo.getChildCount(); ++index) {
			ObjInfo childInfo = objInfo.getChild(index);
			if(childInfo.getExclude())
				continue;
			writer.name(childInfo.getFieldName());
			//if(childInfo.getIndex() >= resultObject.getValueCount()) {
			//	System.out.println("PROBLEM");
			//}
			Object value = resultObject.getValue(childInfo.getIndex());
			if(value == null) {
				writer.nullValue();
				continue;
			}
			switch(childInfo.getType()) {
			case String:	
				try {
					writer.value((String)value);
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				} break;
			case Integer:
			case Decimal:	writer.value((Number)value); break;
			case Boolean:	writer.value((Boolean)value); break;
			case DateTime:	writer.value(Formats.formatLocalDateTime((LocalDateTime)value)); break;
			case Date:		writer.value(Formats.formatLocalDate((LocalDate)value)); break;
			case Time:		writer.value(Formats.formatLocalTime((LocalTime)value)); break;
			case ByteArray:	writer.value(Base64.getUrlEncoder().encodeToString((byte[])value)); break;
			case Object:	writeObject((Obj)value); break;
			case List:		writeList((Obj)value); break;
			default:		
				DataConverter<? extends Object> converter = DataConverter.getConverter(value.getClass(), String.class, false);
				if(converter == null)
					writer.value(value.toString());
				else
					writer.value(converter.convertObject(value, String.class));
				break;
			}
		}
	}

	private void writeList(Obj resultList) throws IOException {
		writer.beginArray();
		for(Object value : resultList.getValues()) {
			writeObject((Obj)value);
		}
		writer.endArray();
	}
	
	@Override
	public void write(int code, String message) throws IOException {
		writer.beginObject();
		if(writeResult) {
			writer.name("ResultCode").value(code);
			writer.name("ResultMessage").value(message);
		}
		writer.endObject();
	}
	
	@Override
	public void flush() throws IOException {
		writer.flush();
	}

	@Override
	public void close() throws IOException {
		writer.close();
	}
}
