package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Locale;
import java.util.TreeSet;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class XsdWriter extends AbstractWriter {

	protected String dataNamespace;
	protected String serviceName;
	protected XMLStreamWriter writer;
	
	protected TreeSet<String> complexTypeNames = new TreeSet<String>();

	public XsdWriter(String dataNamespace, String serviceName, OutputStream out) throws IOException {
		this("text/xsd+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, out);
	}
	
	protected XsdWriter(String contentType, String characterEncoding, String dataNamespace, String serviceName, OutputStream out) throws IOException {
		this(contentType, characterEncoding, dataNamespace, serviceName, new OutputStreamWriter(out, characterEncoding));
	}
	
	public XsdWriter(String dataNamespace, String serviceName, java.io.Writer out) throws IOException {
		this("text/xsd+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, out);
	}

	protected XsdWriter(String contentType, String characterEncoding, String dataNamespace, String serviceName, java.io.Writer out) throws IOException {
		super(contentType, characterEncoding);
		this.dataNamespace = dataNamespace;
		this.serviceName = serviceName;
		try {
			writer = XMLOutputFactory.newInstance().createXMLStreamWriter(out);
			//writer = new IndentingXMLStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(out));
		} catch (XMLStreamException | FactoryConfigurationError e) {
			throw new IOException(e);
		}
	}

	public XsdWriter(String dataNamespace, String serviceName, XMLStreamWriter writer) {
		this("text/xsd+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, writer);
	}

	protected XsdWriter(String contentType, String characterEncoding, String dataNamespace, String serviceName, XMLStreamWriter writer) {
		super(contentType, characterEncoding);
		this.dataNamespace = dataNamespace;
		this.serviceName = serviceName;
		this.writer = writer;
	}
	
	@Override
	public void write(ObjInfoList parameterInfoList, Obj obj) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writeSchema(parameterInfoList, obj);
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	protected void writeSchema(ObjInfoList parameterInfoList, Obj rootResultObject) throws XMLStreamException {
		writer.writeStartElement("xs:schema");
		writer.writeAttribute("targetNamespace", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
		writer.writeAttribute("xmlns:tns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
		writer.writeAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
		writer.writeAttribute("elementFormDefault", "qualified");
		writeTypes(parameterInfoList, rootResultObject);
		writer.writeEndElement();
	}

	protected void writeTypes(ObjInfoList parameterInfoList, Obj rootResultObject) throws XMLStreamException {
		for(ObjInfo parameterInfo : parameterInfoList) {
			if(!parameterInfo.getType().isSimple()) {
				writeComplexType(parameterInfo, true);
				writer.writeStartElement("xs:element");
					writer.writeAttribute("name", parameterInfo.getTypeName());
					writer.writeAttribute("type", "tns:" + parameterInfo.getTypeName());
				writer.writeEndElement();
			}
		}
		
		ObjInfo rootObjInfo = rootResultObject.getInfo();

		writer.writeStartElement("xs:complexType");
			writer.writeAttribute("name", rootObjInfo.getTypeName());
			writer.writeStartElement("xs:sequence");

			if(writeResult) {
				writer.writeStartElement("xs:element");
					writer.writeAttribute("name", "ResultCode");
					writer.writeAttribute("type", "xs:integer");
					writer.writeAttribute("minOccurs", "1");
					writer.writeAttribute("maxOccurs", "1");
				writer.writeEndElement();
	
				writer.writeStartElement("xs:element");
					writer.writeAttribute("name", "ResultMessage");
					writer.writeAttribute("type", "xs:string");
					writer.writeAttribute("minOccurs", "1");
					writer.writeAttribute("maxOccurs", "1");
				writer.writeEndElement();
			}
	
				writeComplexTypeContent(rootObjInfo);
		
			writer.writeEndElement();
		writer.writeEndElement();
		
		writeComplexType(rootObjInfo, false);

		writer.writeStartElement("xs:element");
			writer.writeAttribute("name", rootObjInfo.getTypeName());
			writer.writeAttribute("type", "tns:" + rootObjInfo.getTypeName());
		writer.writeEndElement();
	}
	
	void writeComplexType(ObjInfo parentObjInfo, boolean writeParent) throws XMLStreamException { 
		if(writeParent && !complexTypeNames.contains(parentObjInfo.getTypeName())) {
			writer.writeStartElement("xs:complexType");
			writer.writeAttribute("name", parentObjInfo.getTypeName());
			writer.writeStartElement("xs:sequence");
			writeComplexTypeContent(parentObjInfo);
			writer.writeEndElement();
			writer.writeEndElement();
			complexTypeNames.add(parentObjInfo.getTypeName());
		}

		for(ObjInfo childObjInfo : parentObjInfo.getChilds()) {
			if(!childObjInfo.getType().isSimple())
				writeComplexType(childObjInfo, true);
		}
	}
	
	void writeComplexTypeContent(ObjInfo parentObjInfo) throws XMLStreamException { 
		for(ObjInfo childObjInfo : parentObjInfo.getChilds()) {
			if(childObjInfo.getExclude())
				continue;
			writer.writeStartElement("xs:element");
			if(childObjInfo.getType().isSimple()) {
				writer.writeAttribute("name", childObjInfo.getFieldName());
				writer.writeAttribute("type", childObjInfo.getType().toXsdDataType());
				writer.writeAttribute("maxOccurs", "1");
			}
			else {
				writer.writeAttribute("name", childObjInfo.getFieldName());
				writer.writeAttribute("type", "tns:" + childObjInfo.getTypeName());
				if(childObjInfo.getType() == ObjType.List)
					writer.writeAttribute("maxOccurs", "unbounded");
				else if(childObjInfo.getType() == ObjType.Object)
					writer.writeAttribute("maxOccurs", "1");
			}
			writer.writeEndElement();
		}
	}

	@Override
	public void write(int code, String message) throws IOException {
		try {
			writer.writeStartElement("xs:schema");
			writer.writeAttribute("targetNamespace", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xmlns:tns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");
			writer.writeAttribute("elementFormDefault", "qualified");

			if(writeResult) {
				writer.writeStartElement("Error");
					writer.writeStartElement("ResultCode");
					writer.writeCharacters(String.valueOf(code));
					writer.writeEndElement();
					writer.writeStartElement("ResultMessage");
					writer.writeCharacters(message);
					writer.writeEndElement();
				writer.writeEndElement();
			}
			
			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	@Override
	public void flush() throws IOException {
		try {
			writer.flush();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			writer.close();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
}
