package com.vasnabilisim.olive.server.data.writer;


/**
 * @author Menderes Fatih GUVEN
 */
public abstract class AbstractWriter implements Writer {

	protected String contentType;
	protected String characterEncoding;
	protected boolean writeResult = true;
	
	protected AbstractWriter(String contentType, String characterEncoding) {
		this.contentType = contentType;
		this.characterEncoding = characterEncoding;
	}
	
	@Override
	public String getContentType() {
		return contentType;
	}
	
	@Override
	public String getCharacterEncoding() {
		return characterEncoding;
	}
	
	public boolean getWriteResult() {
		return writeResult;
	}
	
	public void setWriteResult(boolean writeResult) {
		this.writeResult = writeResult;
	}
}
