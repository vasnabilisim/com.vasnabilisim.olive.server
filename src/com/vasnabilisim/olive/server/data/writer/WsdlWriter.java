package com.vasnabilisim.olive.server.data.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public class WsdlWriter extends XsdWriter {

	String dataUrl;
	
	public WsdlWriter(String dataNamespace, String dataUrl, String serviceName, OutputStream out) throws IOException {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, out);
		this.dataUrl = dataUrl;
		this.serviceName = serviceName;
	}
	
	public WsdlWriter(String dataNamespace, String dataUrl, String serviceName, java.io.Writer out) throws IOException {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, out);
		this.dataUrl = dataUrl;
		this.serviceName = serviceName;
	}

	protected WsdlWriter(String dataNamespace, String dataUrl, String serviceName, XMLStreamWriter writer) {
		super("text/soap+xml;charset=utf-8", "UTF-8", dataNamespace, serviceName, writer);
		this.dataUrl = dataUrl;
		this.serviceName = serviceName;
	}
	
	@Override
	public void write(ObjInfoList parameterInfoList, Obj obj) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writeDefinitions(parameterInfoList, obj);
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
	
	void writeDefinitions(ObjInfoList parameterInfoList, Obj obj) throws XMLStreamException {
		writer.writeStartElement("wsdl:definitions");
			writer.writeAttribute("name", serviceName);
			writer.writeAttribute("targetNamespace", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xmlns:wsdl", "http://schemas.xmlsoap.org/wsdl/");
			writer.writeAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
			writer.writeAttribute("xmlns:tns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
			writer.writeAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");

			writer.writeStartElement("wsdl:types");
				writeSchema(parameterInfoList, obj);
			writer.writeEndElement();
			
			writer.writeStartElement("wsdl:message");
				writer.writeAttribute("name", serviceName + "Request");
				for(ObjInfo parameterInfo : parameterInfoList) {
					writer.writeStartElement("wsdl:part");
						writer.writeAttribute("name", parameterInfo.getFieldName());
						if(parameterInfo.getType().isSimple())
							writer.writeAttribute("type", parameterInfo.getType().toXsdDataType());
						else
							writer.writeAttribute("element", "tns:" + parameterInfo.getTypeName());
					writer.writeEndElement();
				}
			writer.writeEndElement();

			writer.writeStartElement("wsdl:message");
				writer.writeAttribute("name", serviceName + "Response");
				writer.writeStartElement("wsdl:part");
					writer.writeAttribute("name", serviceName);
					writer.writeAttribute("element", "tns:" + serviceName);
				writer.writeEndElement();
			writer.writeEndElement();
			
			writer.writeStartElement("wsdl:portType");
				writer.writeAttribute("name", serviceName + "PortType");
				writer.writeStartElement("wsdl:operation");
					writer.writeAttribute("name", "execute"); //TODO find a better name
					writer.writeStartElement("wsdl:input");
						writer.writeAttribute("message", "tns:" + serviceName + "Request");
					writer.writeEndElement();
					writer.writeStartElement("wsdl:output");
						writer.writeAttribute("message", "tns:" + serviceName + "Response");
					writer.writeEndElement();
				writer.writeEndElement();
			writer.writeEndElement();

			writer.writeStartElement("wsdl:binding");
				writer.writeAttribute("name", serviceName + "Service");
				writer.writeAttribute("type", "tns:" + serviceName + "PortType");
				writer.writeStartElement("soap:binding");
					writer.writeAttribute("style", "document");
					writer.writeAttribute("transport", "http://schemas.xmlsoap.org/soap/http");
				writer.writeEndElement();
				writer.writeStartElement("wsdl:operation");
					writer.writeAttribute("name", "execute");
					writer.writeStartElement("soap:operation");
						writer.writeAttribute("soapAction", "execute");
					writer.writeEndElement();
					writer.writeStartElement("wsdl:input");
						writer.writeStartElement("soap:body");
							writer.writeAttribute("use", "literal");
						writer.writeEndElement();
					writer.writeEndElement();
					writer.writeStartElement("wsdl:output");
						writer.writeStartElement("soap:body");
							writer.writeAttribute("use", "literal");
						writer.writeEndElement();
					writer.writeEndElement();
				writer.writeEndElement();
			writer.writeEndElement();

			writer.writeStartElement("wsdl:service");
				writer.writeAttribute("name", serviceName + "Service");
				writer.writeStartElement("wsdl:port");
					writer.writeAttribute("binding", "tns:" + serviceName + "Service");
					writer.writeAttribute("name", serviceName);
					writer.writeStartElement("soap:address");
						writer.writeAttribute("location", dataUrl + "/ws/" + serviceName);
					writer.writeEndElement();
				writer.writeEndElement();
			writer.writeEndElement();
		
		writer.writeEndElement();
	}
	
	@Override
	public void write(int code, String message) throws IOException {
		try {
			writer.writeStartDocument("UTF-8", "1.0");
			writer.writeStartElement("wsdl:definitions");
				writer.writeAttribute("name", serviceName);
				writer.writeAttribute("targetNamespace", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
				writer.writeAttribute("xmlns:wsdl", "http://schemas.xmlsoap.org/wsdl/");
				writer.writeAttribute("xmlns:soap", "http://schemas.xmlsoap.org/wsdl/soap/");
				writer.writeAttribute("xmlns:tns", dataNamespace + "/" + serviceName.toLowerCase(Locale.US));
				writer.writeAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema");

				if(writeResult) {
					writer.writeStartElement("Error");
						writer.writeStartElement("ResultCode");
						writer.writeCharacters(String.valueOf(code));
						writer.writeEndElement();
						writer.writeStartElement("ResultMessage");
						writer.writeCharacters(message);
						writer.writeEndElement();
					writer.writeEndElement();
				}

			writer.writeEndElement();
			writer.writeEndDocument();
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}
}
