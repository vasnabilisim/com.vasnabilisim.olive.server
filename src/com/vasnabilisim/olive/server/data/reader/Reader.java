package com.vasnabilisim.olive.server.data.reader;

import java.io.IOException;

import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public interface Reader {

	/**
	 * Read parameters.
	 * @param parameterInfoList
	 * @throws IOException
	 */
	public void read(ObjInfoList parameterInfoList) throws IOException;
}
