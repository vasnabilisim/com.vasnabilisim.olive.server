package com.vasnabilisim.olive.server.data.reader;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public class WsReader extends XmlReader implements Reader {

	public WsReader(InputStream in) throws IOException {
		super(in);
	}

	public WsReader(java.io.Reader in) throws IOException {
		super(in);
	}

	public WsReader(XMLStreamReader reader) throws IOException {
		super(reader);
	}
	
	@Override
	public void read(ObjInfoList parameterInfoList) throws IOException {
		try {
			skipTillElement();
			int event = reader.getEventType();
			if(event != XMLStreamConstants.START_ELEMENT)
				throw new IOException("Unable to find element Envelope");
			String localName = reader.getLocalName();
			if(!"Envelope".equals(localName)) {
				String message = String.format("Expecting Envelope found %s", localName);
				throw new IOException(message);
			}
			while((event = reader.next()) != XMLStreamConstants.END_ELEMENT) {
				if(event == XMLStreamConstants.START_ELEMENT) {
					localName = reader.getLocalName();
					if("Header".equals(localName)) {
						skipElement();
						continue;
					}
					if("Body".equals(localName))
						break;
					String message = String.format("Unexpected element %s", localName);
					throw new IOException(message);
				}
			}
			readInternal(parameterInfoList);
			//May have more document
			while(reader.hasNext() && (event = reader.next()) != XMLStreamConstants.END_ELEMENT) {
				if(event == XMLStreamConstants.START_ELEMENT) {
					localName = reader.getLocalName();
					String message = String.format("Unexpected element %s", localName);
					throw new IOException(message);
				}
			} //End of envelope
		} catch (XMLStreamException e) {
			throw new IOException(e.getMessage(), e);
		}
	}
}
