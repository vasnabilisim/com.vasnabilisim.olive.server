package com.vasnabilisim.olive.server.data.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class XmlReader implements Reader {

	XMLStreamReader reader;
	
	public XmlReader(InputStream in) throws IOException {
		this(new InputStreamReader(in, "UTF-8"));
	}

	public XmlReader(java.io.Reader in) throws IOException {
		try {
			reader = XMLInputFactory.newInstance().createXMLStreamReader(in);
		} catch (XMLStreamException | FactoryConfigurationError e) {
			throw new IOException(e);
		}
	}

	public XmlReader(XMLStreamReader reader) throws IOException {
		this.reader = reader;
	}
	
	@Override
	public void read(ObjInfoList parameterInfoList) throws IOException {
		try {
			skipTillElement();
			int event = reader.getEventType();
			if(event != XMLStreamConstants.START_ELEMENT)
				throw new IOException("Unable to find element Parameters");
			String localName = reader.getLocalName();
			if(!"Parameters".equals(localName)) {
				String message = String.format("Expecting Parameters found %s", localName);
				throw new IOException(message);
			}
			readInternal(parameterInfoList);
		} catch (XMLStreamException e) {
			throw new IOException(e.getMessage(), e);
		}
	}
	
	void readInternal(ObjInfoList parameterInfoList) throws IOException {
		try {
			skipTillElement();
			int event = reader.getEventType();
			if(event != XMLStreamConstants.START_ELEMENT)
				return;
			while(true) {
				String localName = reader.getLocalName();
				ObjInfo parameterInfo = parameterInfoList.getObjInfo(localName);
				if(parameterInfo == null) {
					String message = String.format("Unexpected element %s", localName);
					throw new IOException(message);
				}
				Object parameterValue = readInternal(parameterInfo);
				parameterInfo.setValue(parameterValue);
				boolean done = true;
				while(reader.hasNext()) {
					event = reader.next();
					if(event == XMLStreamConstants.START_ELEMENT) {
						done = false;
						break;
					}
					if(event == XMLStreamConstants.END_ELEMENT)
						break;
				}
				if(done)
					break;
			}
		} catch (XMLStreamException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	Object readInternal(ObjInfo objInfo) throws XMLStreamException, IOException {
		if(objInfo.getType() == ObjType.Object)
			return readObject(objInfo);
		if(objInfo.getType() == ObjType.List)
			return readList(objInfo);
		return readSimple(objInfo.getType());
	}

	Obj readObject(ObjInfo objInfo) throws XMLStreamException, IOException {
		Obj obj = new Obj(objInfo);
		obj.fillNull();
		int event;
		while((event = reader.next()) != XMLStreamConstants.END_ELEMENT) {
			if(event == XMLStreamConstants.START_ELEMENT) {
				String localName = reader.getLocalName();
				ObjInfo childInfo = objInfo.getChild(localName);
				if(childInfo == null) {
					String message = String.format("Unexpected element %s", localName);
					throw new IOException(message);
				}
				Object childValue = readInternal(childInfo);
				obj.setValue(childInfo.getIndex(), childValue);
			}
		}
		return obj;
	}
	
	Obj readList(ObjInfo objInfo) throws XMLStreamException, IOException {
		//TODO implement
		return null;
	}

	Object readSimple(ObjType type) throws XMLStreamException, IOException {
		int event = reader.getEventType();
		String stringValue = "";
		while((event = reader.next()) != XMLStreamConstants.END_ELEMENT) {
			if(event == XMLStreamConstants.CHARACTERS)
				stringValue += reader.getText();
		}
		return type.fromStringToObject(stringValue);
	}

	void skipTillElement() throws XMLStreamException {
		while(reader.hasNext()) {
			if(reader.next() == XMLStreamConstants.START_ELEMENT)
				break;
		}
	}

	void skipElement() throws XMLStreamException {
		int event;
		while(reader.hasNext()) {
			event = reader.next();
			if(event == XMLStreamConstants.START_ELEMENT) {
				skipElement();
				continue;
			}
			if(event == XMLStreamConstants.END_ELEMENT)
				break;
		}
	}
	
	String fromEventToString(int event) {
		switch (event) {
		case XMLStreamConstants.START_ELEMENT:			return "START_ELEMENT";
		case XMLStreamConstants.END_ELEMENT:			return "END_ELEMENT";
		case XMLStreamConstants.PROCESSING_INSTRUCTION:	return "PROCESSING_INSTRUCTION";
		case XMLStreamConstants.CHARACTERS:				return "CHARACTERS";
		case XMLStreamConstants.COMMENT:				return "COMMENT";
		case XMLStreamConstants.SPACE:					return "SPACE";
		case XMLStreamConstants.START_DOCUMENT:			return "START_DOCUMENT";
		case XMLStreamConstants.END_DOCUMENT:			return "END_DOCUMENT";
		case XMLStreamConstants.ENTITY_REFERENCE:		return "ENTITY_REFERENCE";
		case XMLStreamConstants.ATTRIBUTE:				return "ATTRIBUTE";
		case XMLStreamConstants.DTD:					return "DTD";
		case XMLStreamConstants.CDATA:					return "CDATA";
		case XMLStreamConstants.NAMESPACE:				return "NAMESPACE";
		case XMLStreamConstants.NOTATION_DECLARATION:	return "NOTATION_DECLARATION";
		case XMLStreamConstants.ENTITY_DECLARATION:		return "ENTITY_DECLARATION";
		default: return "unknown";
		}
	}
	
}
