package com.vasnabilisim.olive.server.data.reader;

import java.io.IOException;

import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;

/**
 * @author Menderes Fatih GUVEN
 */
public class XsdReader implements Reader {

	public XsdReader() throws IOException {
	}

	@Override
	public void read(ObjInfoList parameterInfoList) throws IOException {
		for(ObjInfo objInfo : parameterInfoList)
			objInfo.getValue();
	}
}
