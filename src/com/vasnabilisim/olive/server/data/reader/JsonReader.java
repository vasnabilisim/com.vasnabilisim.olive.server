package com.vasnabilisim.olive.server.data.reader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.Base64;

import com.google.gson.stream.JsonToken;
import com.vasnabilisim.olive.core.Formats;
import com.vasnabilisim.olive.core.model.Obj;
import com.vasnabilisim.olive.core.model.ObjInfo;
import com.vasnabilisim.olive.core.model.ObjInfoList;
import com.vasnabilisim.olive.core.model.ObjType;

/**
 * @author Menderes Fatih GUVEN
 */
public class JsonReader implements Reader {

	com.google.gson.stream.JsonReader reader;
	
	public JsonReader(InputStream in) throws IOException {
		this(new InputStreamReader(in, "UTF-8"));
	}

	public JsonReader(java.io.Reader in) throws IOException {
		reader = new com.google.gson.stream.JsonReader(in);
		reader.setLenient(true);
	}

	public JsonReader(com.google.gson.stream.JsonReader reader) throws IOException {
		this.reader = reader;
	}

	@Override
	public void read(ObjInfoList parameterInfoList) throws IOException {
		JsonToken jsonToken = reader.peek();
		if(jsonToken != JsonToken.BEGIN_OBJECT)
			return;
		try {
			reader.beginObject();
			while(reader.hasNext()) {
				String parameterName = reader.nextName();			
				ObjInfo parameterInfo = parameterInfoList.getObjInfo(parameterName);
				if(parameterInfo == null) {
					reader.skipValue();
					//Logger.warning(String.format("Unexpected parameter %s", parameterName));
					continue;
				}
				Object parameterValue = readInternal(parameterInfo);
				parameterInfo.setValue(parameterValue);
			}
			reader.endObject();
		} catch (IllegalStateException e) {
			throw new IOException(e.getMessage(), e);
		}
	}

	Object readInternal(ObjInfo objInfo) throws IllegalStateException, IOException {
		JsonToken jsonToken = reader.peek();
		if(!canConvert(jsonToken, objInfo.getType())) {
			String message = String.format("Connot convert json:%s to obj:%s", jsonToken.name(), objInfo.getType().name());
			throw new IOException(message);
		}
		/*
		JsonToken expectedJsonToken = toJsonToken(objInfo.getType());
		if(jsonToken != expectedJsonToken) {
			String message = String.format("Expecting %s found %s", expectedJsonToken.name(), jsonToken.name());
			throw new IOException(message);
		}
		*/
		switch (objInfo.getType()) {
		case String:	return nextString();
		case Integer:	return nextLong();
		case Decimal:	return nextDecimal();
		case Boolean:	return nextBoolean();
		case DateTime:	return nextDateTime();
		case Date:		return nextDate();
		case Time:		return nextTime();
		case ByteArray:	return nextByteArray();
		case Object: {
			Obj obj = new Obj(objInfo);
			//Set all child values to null
			obj.setValue(objInfo.getChildCount() - 1, null);
			reader.beginObject();
			while(reader.hasNext()) {
				ObjInfo childObjInfo = objInfo.getChild(reader.nextName());
				if(childObjInfo == null) {
					reader.skipValue();
					continue;
				}
				obj.setValue(childObjInfo.getIndex(), readInternal(childObjInfo));
			}
			reader.endObject();
			return obj;
		}
		case List: {
			Obj obj = new Obj(objInfo);
			ObjInfo childObjInfo = objInfo.toObjInfo(ObjType.Object);
			reader.beginArray();
			while(reader.hasNext()) {
				obj.addValue(readInternal(childObjInfo));
			}
			reader.endArray();
			return obj;
		}
		default:
			return null;
		}
		
	}
	
	String nextString() throws IOException {
		return reader.nextString();
	}
	
	Long nextLong() throws IOException {
		try {
			return Long.valueOf(reader.nextString());
		} catch(NumberFormatException e) {
			return null;
		}
	}
	
	BigDecimal nextDecimal() throws IOException {
		try {
			return new BigDecimal(reader.nextString());
		} catch(NumberFormatException e) {
			return null;
		}
	}
	
	Boolean nextBoolean() throws IOException {
		return reader.nextBoolean();
	}
	
	LocalDateTime nextDateTime() throws IOException {
		try {
			return LocalDateTime.parse(reader.nextString(), Formats.LocalDateTimeFormatter);
		} catch(DateTimeParseException e) {
			return null;
		}
	}
	
	LocalDate nextDate() throws IOException {
		String stringValue = reader.nextString();
		try {
			return LocalDate.parse(stringValue, Formats.LocalDateFormatter);
		} catch(DateTimeParseException e) {
			try {
				return LocalDate.parse(stringValue, Formats.LocalDateTimeFormatter);
			} catch(DateTimeParseException e2) {
				return null;
			}
		}
	}
	
	LocalTime nextTime() throws IOException {
		String stringValue = reader.nextString();
		try {
			return LocalTime.parse(stringValue, Formats.LocalTimeFormatter);
		} catch(DateTimeParseException e) {
			try {
				return LocalTime.parse(stringValue, Formats.LocalDateTimeFormatter);
			} catch(DateTimeParseException e2) {
				return null;
			}
		}
	}
	
	byte[] nextByteArray() throws IOException {
		try {
			return Base64.getUrlDecoder().decode(reader.nextString());
		} catch(IllegalArgumentException e) {
			return null;
		}
	}
	
	JsonToken toJsonToken(ObjType objType) {
		switch (objType) {
		case String:	return JsonToken.STRING;
		case Integer:	return JsonToken.NUMBER;
		case Decimal:	return JsonToken.NUMBER;
		case Boolean:	return JsonToken.BOOLEAN;
		case DateTime:	return JsonToken.STRING;
		case Date:		return JsonToken.STRING;
		case Time:		return JsonToken.STRING;
		case ByteArray:	return JsonToken.STRING;
		case Object:	return JsonToken.BEGIN_OBJECT;
		case List:		return JsonToken.BEGIN_ARRAY;
		default:		throw new UnsupportedOperationException(String.format("Unsupported obj type %s", objType.name()));
		}
	}
	
	boolean canConvert(JsonToken jsonToken, ObjType objType) {
		switch (objType) {
		case String:	return jsonToken == JsonToken.STRING || jsonToken == JsonToken.NUMBER || jsonToken == JsonToken.BOOLEAN;
		case Integer:	return jsonToken == JsonToken.STRING || jsonToken == JsonToken.NUMBER;
		case Decimal:	return jsonToken == JsonToken.STRING || jsonToken == JsonToken.NUMBER;
		case Boolean:	return jsonToken == JsonToken.STRING || jsonToken == JsonToken.BOOLEAN;
		case DateTime:	return jsonToken == JsonToken.STRING;
		case Date:		return jsonToken == JsonToken.STRING;
		case Time:		return jsonToken == JsonToken.STRING;
		case ByteArray:	return jsonToken == JsonToken.STRING;
		case Object:	return jsonToken == JsonToken.BEGIN_OBJECT;
		case List:		return jsonToken == JsonToken.BEGIN_ARRAY;
		default: 		return false;
		}
	}
}
