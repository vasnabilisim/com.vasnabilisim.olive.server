package com.vasnabilisim.olive.server;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.vasnabilisim.core.BaseException;
import com.vasnabilisim.core.Logger;
import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.olive.core.model.sql.SqlModelInfo;
import com.vasnabilisim.olive.core.model.xmlmodel.XmlModelType;
import com.vasnabilisim.olive.server.data.handler.SqlModelHandler;
import com.vasnabilisim.olive.server.data.writer.Writer;

/**
 * @author Menderes Fatih GUVEN
 */
public class SqlModelDataHandler extends AbstractDataHandler {

	protected void handle(String formatName, String modelName, Map<String, String> parameterMap, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		File file = new File(ApplicationContext.getString("Olive.Server.SqlModelPath", true) + "/" + modelName + ".xml");
		if(!file.exists()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			response.getWriter().write(String.format("Invalid path. Unable to find [%s]", modelName));
			response.getWriter().close();
			return;
		}		
		SqlModelInfo sqlModelInfo;
		try {
			sqlModelInfo = (SqlModelInfo) XmlModelType.SqlModel.read(file);
		} catch (BaseException e) {
			Logger.error(e);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().write("Unexpected exception");
			response.getWriter().close();
			return;
		}
		
		handle(formatName, new SqlModelHandler(sqlModelInfo), parameterMap, baseRequest, request, response);
	}

	@Override
	Writer createWriter(String format, String handlerName, HttpServletResponse response) throws IOException {
		Writer writer = super.createWriter(format, handlerName, response);
		writer.setWriteResult(false);
		return writer;
	}
}
