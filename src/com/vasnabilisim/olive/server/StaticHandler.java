package com.vasnabilisim.olive.server;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.vasnabilisim.io.IOUtil;
import com.vasnabilisim.io.MimeType;

/**
 * @author Menderes Fatih GUVEN
 */
public class StaticHandler extends AbstractHandler {

	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		baseRequest.setHandled(true);

		String path = baseRequest.getPathInfo();
		path = path.substring(1); //remove '/'
		
		transfer(response, path);
	}

	static void transfer(HttpServletResponse response, String path)
			throws IOException {
		File file = new File(path);
		if(!file.exists()) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		
		String fileExtension = IOUtil.findFileExtension(path);
		if(fileExtension == null)
			response.setContentType("text/unknown");
		else 
			response.setContentType(MimeType.contentTypeOfFileExtension(fileExtension));
		
		TransferCallable transferCallable = new TransferCallable(file, response.getOutputStream());
		try {
			transferCallable.transfer();
		} catch (InterruptedException | ExecutionException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} catch (TimeoutException e) {
			response.setStatus(HttpServletResponse.SC_REQUEST_TIMEOUT);
		}
	}

	static class TransferCallable implements Callable<Boolean> {
		File file;
		OutputStream out;
		TransferCallable(File file, OutputStream out) {
			this.file = file;
			this.out = out;
		}
		@Override
		public Boolean call() throws Exception {
			IOUtil.transfer(file, out);
			return Boolean.TRUE;
		}
		
		public void transfer() throws InterruptedException, ExecutionException, TimeoutException {
			ServerExecutorService.execute(this);
		}
	}
}
