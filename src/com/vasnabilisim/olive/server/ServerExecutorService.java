package com.vasnabilisim.olive.server;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.vasnabilisim.core.ApplicationContext;

/**
 * @author Menderes Fatih GUVEN
 */
public final class ServerExecutorService {

	private ServerExecutorService() {
	}

	static ExecutorService executorService;
	static long timeOutSeconds;
	public static ExecutorService getExecutorService() {
		if(executorService == null) {
			executorService = Executors.newCachedThreadPool();
			timeOutSeconds =  Long.parseLong(ApplicationContext.getString("Olive.Server.RequestTimeOutSeconds", "10"));
		}
		return executorService;
	}

	public static <R> R execute(Callable<R> callable) throws InterruptedException, ExecutionException, TimeoutException {
		Future<R> futureTask = getExecutorService().submit(callable);
		return futureTask.get(timeOutSeconds, TimeUnit.SECONDS);
	}
}
