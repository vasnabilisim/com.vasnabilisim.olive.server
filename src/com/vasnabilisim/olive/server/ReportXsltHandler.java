package com.vasnabilisim.olive.server;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import com.vasnabilisim.core.ApplicationContext;
import com.vasnabilisim.olive.server.data.writer.Writer;
import com.vasnabilisim.olive.server.data.writer.XmlWriter;
import com.vasnabilisim.util.StringUtil;

/**
 * @author Menderes Fatih GUVEN
 */
public class ReportXsltHandler extends DataHandler {

	String xsltName;
	
	@Override
	public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		baseRequest.setHandled(true);

		String path = baseRequest.getPathInfo();
		if(StringUtil.isEmpty(path) || path.length() == 1) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		Map<String, String> parameterMap = toParameterMap(request.getParameterMap());
		
		path = path.substring(1); //remove '/'
		String dataHandlerName = path;
		xsltName = parameterMap.get("design");
		if(xsltName == null)
			StaticHandler.transfer(response, ApplicationContext.getString("Olive.Server.XsltPath", true) + "/" + path);
		else
			handle("xml", dataHandlerName, parameterMap, baseRequest, request, response);
	}
	
	Writer createWriter(String format, String handlerName, HttpServletResponse response) throws IOException {
		String dataPath = ApplicationContext.getString("Olive.Server.UrlDataPath", true);
		String dataNamespace = ApplicationContext.getString("Olive.Server.Namespace", true) + dataPath;
		String dataUrl = ApplicationContext.getString("Olive.Server.Url", true) + dataPath;
		String xsltUrl = null;
		if(xsltName != null) {
			xsltUrl = xsltName + ".xslt";
		}
		return new XmlWriter(dataNamespace, dataUrl, xsltUrl, handlerName, response.getOutputStream());
	}
}
